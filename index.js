function tinhDiemDuaVaoKhuVuc(khuVuc) {
	switch (khuVuc) {
		case 'A': {
			return 2;
			break;
		}
		case 'B': {
			return 1;
			break;
		}
		case 'C': {
			return 0.5;
			break;
		}
		case 'X': {
			return 0;
			break;
		}
	}
}

function tinhDiemDuaVaoDoiTuong(doiTuong) {
	switch (doiTuong) {
		case '0': {
			return 0;
			break;
		}
		case '1': {
			return 2.5;
			break;
		}
		case '2': {
			return 1.5;
			break;
		}
		case '3': {
			return 1;
			break;
		}
	}
}

function tinhDiem() {
	var select = document.getElementById('khuvuc');
	var khuVuc = select.options[select.selectedIndex].value;
	var diemKhuVuc = tinhDiemDuaVaoKhuVuc(khuVuc);
	console.log('diemKhuVuc: ', diemKhuVuc);
	var select1 = document.getElementById('doituong');
	var doiTuong = select1.options[select.selectedIndex].value;
	var diemDoiTuong = tinhDiemDuaVaoDoiTuong(doiTuong);
	console.log('diemDoiTuong: ', diemDoiTuong);

	var diemChuan = document.getElementById('diemchuan').value * 1;
	var diemMonThuNhat = document.getElementById('diemmon1').value * 1;
	var diemMonThuHai = document.getElementById('diemmon2').value * 1;
	var diemMonThuBa = document.getElementById('diemmon3').value * 1;
	var diemUuTien = diemDoiTuong + diemKhuVuc;
	console.log('diemUuTien: ', diemUuTien);
	var tongDiem = diemUuTien + diemMonThuNhat + diemMonThuHai + diemMonThuBa;
	if (diemChuan < tongDiem) {
		document.getElementById('result').innerHTML = `Bạn đã đậu.Tổng điểm ${tongDiem}`;
	} else
		document.getElementById('result').innerHTML = `Bạn đã rớt. Tổng điểm ${tongDiem}`;
}


function tinhTienDien() {
	const giaTien50KWDauTien = 500;
	const giaTien50KwKe = 650;
	const giaTien100KwKe = 850;
	const giaTien150KwKe = 1100;
	const giaTienTren150Kw = 1300;
	var tenNguoiDung = document.getElementById('hoten').value;
	var soKw = document.getElementById('sokw').value * 1;
	var tienDien = 0;

	if (soKw <= 50) {
		tienDien = giaTien50KWDauTien * soKw;
	} else if (soKw > 50 && soKw <= 100) {
		tienDien = giaTien50KWDauTien * 50 + (soKw - 50) * giaTien50KwKe;
	} else if (soKw > 100 && soKw <= 200) {
		tienDien = giaTien50KWDauTien * 50 + giaTien50KwKe * 50 + giaTien100KwKe * (200 - soKw);
	} else if (soKw > 200 && soKw <= 350) {
		tienDien = giaTien50KWDauTien * 50 + giaTien50KwKe * 50 + giaTien100KwKe * 100 + giaTien150KwKe * (350 - soKw);
	} else if (soKw > 350) {
		tienDien = giaTien50KWDauTien * 50 + giaTien50KwKe * 50 + giaTien100KwKe * 100 + giaTien150KwKe * 150 + (soKw - 350) * giaTienTren150Kw;
	}
	var result = document.getElementById('result1').innerHTML = `Họ tên:${tenNguoiDung};Tiền điện:${tienDien}`;
}